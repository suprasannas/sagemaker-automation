import boto3

trainingJobName = "xgboost-2019-05-09-15-20-51-276" 

print(trainingJobName)

sm = boto3.client("sagemaker")

job = sm.describe_training_job(TrainingJobName=trainingJobName)
trainingImage = job['AlgorithmSpecification']['TrainingImage']
modelDataUrl  = job['ModelArtifacts']['S3ModelArtifacts']
roleArn       = job['RoleArn']

cf = boto3.client("cloudformation")

with open("endpoint-one-model.yml", "r") as f:
	stack = cf.create_stack(StackName="endpoint-one-model", 
		   TemplateBody=f.read(),
		   Parameters=[
			{"ParameterKey":"ModelName",     "ParameterValue":trainingJobName},
			{"ParameterKey":"TrainingImage", "ParameterValue":trainingImage},
			{"ParameterKey":"ModelDataUrl",  "ParameterValue":modelDataUrl},
			{"ParameterKey":"RoleArn",       "ParameterValue":roleArn} ])
	print(stack)

