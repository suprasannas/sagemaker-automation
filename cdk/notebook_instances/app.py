from aws_cdk import (
    aws_sagemaker as sagemaker,
    aws_s3 as s3,
    core
)

class SagemakerStack(core.Stack):
    def __init__(self, app: core.App, id: str, **kwargs) -> None:
        super().__init__(app, id, **kwargs)

        instance = sagemaker.CfnNotebookInstance( 
          scope=self,
	  id="My instance",
          instance_type="ml.t2.large",
          role_arn=self.node.try_get_context("role_arn"),
	  default_code_repository="https://gitlab.com/juliensimon/dlnotebooks"
        )

        #bucket = s3.Bucket(scope=self, id="My bucket")

app = core.App()
SagemakerStack(app, "SagemakerStackUS", env={'region': 'us-east-1'})
SagemakerStack(app, "SagemakerStackEU", env={'region': 'eu-west-1'})
app.synth()
